# Reflow Input Device
Software and Hardware concerning *Botto* electronic device for the project Reflow.

## General info
- User Flow in Figma: https://www.figma.com/file/BOVXg2KYEAavzzHjhHDypT/Reflow-Prototipo?node-id=0%3A1

## To Do 
Updated to 13/01/2022
- Software	
	- Develop
		- OS
			- Time loop
			- FSM			
			- GraphQL calls
			- Interactions
				- Input reading
				- LED outputs
					- Colors
					- Messages
				- Buzzer
				- LED
			- Debug features
			- Control over serial